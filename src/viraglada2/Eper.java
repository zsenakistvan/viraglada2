package viraglada2;

import annotation.getterFunctionName;
import java.util.AbstractMap;

public class Eper extends Noveny{
    @getterFunctionName(value = "getIz", type = String.class)
    private String iz;

    public Eper() {
        super();
        this.iz = "íztelen";
    }

    public Eper(String iz, String fajta, SZINEK szin, AbstractMap.SimpleEntry<Integer, String> illat) {
        super(fajta, szin, illat);
        this.iz = iz;
    }

    public String getIz() {
        return this.iz;
    }

    public void setIz(String iz) {
        this.iz = iz;
    }
    
    
    
    
    
    
}
