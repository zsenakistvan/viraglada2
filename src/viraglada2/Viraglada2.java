package viraglada2;

import annotation.MinMaxValue;
import java.lang.reflect.Field;
import java.util.Arrays;

public class Viraglada2 {
    
    public static void main(String[] args) {
        
        Virag v = new Virag();
        Virag i = new Virag();
        
        System.out.println(v.getFajta());
        //v.fajta = "Tulipán";
        System.out.println(v.getFajta());
        System.out.println(i.getFajta());
        //i.fajta = "Rózsa";
        System.out.println(i.getFajta());
        System.out.println(i.getPorzoSzam());
        
        Virag r = new Virag("Kaktusz", 12, SZINEK.LILA, Boolean.TRUE, ILLAT.TAV);
        System.out.println("A(z) " + r.getFajta() + " színe: " + r.getSzin());
        System.out.println("A(z) " + r.getSzin() + " RED csatornája: " + r.getSzin().red());
        System.out.println("A(z) " + r.getSzin() + " GREEN csatornája: " + r.getSzin().green());
        System.out.println("A(z) " + r.getSzin() + " BLUE csatornája: " + r.getSzin().blue());
        System.out.println("A(z) " + r.getFajta() + " illata: " + r.getIllat().getValue() + ", aminek a kódja: " + r.getIllat().getKey());
        
        System.out.println(r.getSzin());
        r.porzoLetepes(5);
        System.out.println(r.getPorzoSzam());
        
        System.out.println(i.isKinyilt());
        i.kinyilik();
        System.out.println(i.isKinyilt());
        
        
        //ILLAT dgh = new ILLAT();
        
        Virag g = new Virag("Rózsa", 10, SZINEK.PIROS, Boolean.FALSE, ILLAT.ÉDES);
        System.out.println(g.getPorzoSzam());
        Integer ujPorzok = 32;
        Field[] fields = Virag.class.getDeclaredFields();
        Integer max = 0;
        for(Field f : fields){
            if(f.getName().equals("porzoSzam")){
                max = f.getAnnotation(MinMaxValue.class).max();
            }
        }
        if(g.getPorzoSzam() + ujPorzok <= max){
            g.pluszPorzo(ujPorzok);
        }
        else{
            System.out.println("Nem lehet több porzója egy virágnak, mint " + max + "!");
        }
        System.out.println(g.getPorzoSzam());
        
        Eper e = new Eper();
        e.setIllat(ILLAT.SAV);
        e.setIz("savanyú");
        e.setSzin(SZINEK.ZÖLD);
       
        Funkciok<Eper> f = new Funkciok();
        //f.mentes(e);
        System.out.println("________________________");
        Funkciok<Virag> k = new Funkciok<Virag>();
        //k.mentes(v);
        f.betoltes(e);
        
    }
    
}