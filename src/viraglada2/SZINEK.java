package viraglada2;
public enum SZINEK {
    PIROS(255, 0, 0), 
    ZÖLD(0, 255, 0), 
    SÁRGA(255, 255, 0), 
    FEKETE(0, 0, 0), 
    FEHÉR(255, 255, 255), 
    KÉK(0, 0, 255), 
    LILA(255, 0, 255);
    
    private Integer red;
    private Integer green;
    private Integer blue;

    private SZINEK(Integer red, Integer green, Integer blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public Integer red() {
        return this.red;
    }

    public Integer green() {
        return this.green;
    }

    public Integer blue() {
        return this.blue;
    }
    
    
    
}