package viraglada2;

import annotation.MinMaxValue;
import annotation.getterFunctionName;
import java.util.AbstractMap.SimpleEntry;

public class Virag extends Noveny{
    
    @MinMaxValue(min = 0, max = 20)
    @getterFunctionName(value = "getPorzoSzam", type = Integer.class)
    private Integer porzoSzam;
    //private Integer maxPorzoSzam = 20;
    @getterFunctionName(value = "isKinyilt", type = Boolean.class)
    private Boolean kinyilt;
    
    public Virag(){
        super();
        this.porzoSzam = 0;
        this.kinyilt = Boolean.FALSE; // = false
        
    }
    
    public Virag(String fajta, Integer porzoSzam, SZINEK szin, Boolean kinyilt, SimpleEntry<Integer, String> illat){
        super(fajta, szin, illat);
        this.kinyilt = kinyilt;
        this.porzoSzam = 0;
        try{
            if(porzoSzam <= 
                    Virag.class.getDeclaredField("porzoSzam").getAnnotation(MinMaxValue.class).max() 
                    && 
               porzoSzam >= Virag.class.getDeclaredField("porzoSzam").getAnnotation(MinMaxValue.class).min()) {
                this.porzoSzam = porzoSzam;
            }
        }
        catch(Exception ex){
            System.out.println("Hiba: " + ex.toString());
        }
    }
    
    public Integer getPorzoSzam(){
        return this.porzoSzam;
    }

    public Boolean isKinyilt() {
        return this.kinyilt;
    }
    
    public void kinyilik(){
        this.kinyilt = Boolean.TRUE;
    }
    
    public void pluszPorzo(Integer ujPorzokSzama){
        //max 20 porzója van a virágoknak!
        try{
            if(this.porzoSzam + ujPorzokSzama <= Virag.class.getDeclaredField("porzoSzam").getAnnotation(MinMaxValue.class).max()){
                this.porzoSzam += ujPorzokSzama;
            }
        }
        catch(Exception ex){}
    }
    
    public void porzoLetepes(Integer letepettPorzokSzama){
        try{
            if(this.porzoSzam - letepettPorzokSzama >= Virag.class.getDeclaredField("porzoSzam").getAnnotation(MinMaxValue.class).min()){
                this.porzoSzam -= letepettPorzokSzama;
            }
        }
        catch(Exception ex){}
    }
    
    
    
}
