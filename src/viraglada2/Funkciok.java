package viraglada2;

import annotation.getterFunctionName;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Funkciok<Osztaly> {
    
    public Boolean mentes(Osztaly peldany){
        try{
            String fajlnev = peldany.getClass().getSimpleName() + ".xml";
            File f = new File(fajlnev);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            TransformerFactory tf = TransformerFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Transformer t = tf.newTransformer();
            if(!(f.exists() && f.isFile())){
                f.createNewFile();
                Document xml = db.newDocument();
                xml.setXmlVersion("1.0");
                t.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                String foelemNev = peldany.getClass().getName();
                Element foelem = xml.createElement(foelemNev);
                xml.appendChild(foelem);
                DOMSource source = new DOMSource(xml);
                StreamResult result = new StreamResult(f);
                t.transform(source, result);
            }
            Map<String, String[]> adatok = new HashMap<>();
            Field[] tulajdonsagok = peldany.getClass().getDeclaredFields();
            for(Field tul : tulajdonsagok){
                String tulajdonsagNev = tul.getName();
                String metodusNev = tul.getAnnotation(getterFunctionName.class).value();
                String metodusTipus = tul.getAnnotation(getterFunctionName.class).type().getSimpleName();
                Method getterFuggveny = peldany.getClass().getMethod(metodusNev);
                String tulajdonsagErtek = getterFuggveny.invoke(peldany).toString();
                String[] ertekEsTipus = { tulajdonsagErtek, metodusTipus };
                adatok.put(tulajdonsagNev, ertekEsTipus);
                //System.out.println(tulajdonsagNev + ": " + tulajdonsagErtek);
            }
            
            tulajdonsagok = peldany.getClass().getSuperclass().getDeclaredFields();
            for(Field tul : tulajdonsagok){
                String tulajdonsagNev = tul.getName();
                String metodusNev = tul.getAnnotation(getterFunctionName.class).value();
                String metodusTipus = tul.getAnnotation(getterFunctionName.class).type().getSimpleName();
                Method getterFuggveny = peldany.getClass().getSuperclass().getMethod(metodusNev);
                String tulajdonsagErtek = getterFuggveny.invoke(peldany).toString();
                String[] ertekEsTipus = { tulajdonsagErtek, metodusTipus };
                adatok.put(tulajdonsagNev, ertekEsTipus);
                //System.out.println(tulajdonsagNev + ": " + tulajdonsagErtek);
            }
            Document xml = db.parse(f);
            Element ujElem = xml.createElement(peldany.getClass().getSimpleName());
            Element foelem = (Element)xml.getFirstChild();
            foelem.appendChild(ujElem);
            for(Map.Entry<String, String[]> adat : adatok.entrySet()){
                Element tulajdonsag = xml.createElement(adat.getKey());
                tulajdonsag.setTextContent(adat.getValue()[0]);
                tulajdonsag.setAttribute("type", adat.getValue()[1]);
                ujElem.appendChild(tulajdonsag);
            }
            DOMSource source = new DOMSource(xml);
            StreamResult result = new StreamResult(f);
            t.transform(source, result);
            return Boolean.TRUE;
        }
        catch(Exception ex){
            System.out.println(ex.toString());
        }
        return Boolean.FALSE;
    }
    
    public Object[] betoltes(Osztaly peldany){
        Object[] egyedek = null;
        String fajlnev = peldany.getClass().getSimpleName() + ".xml";
        try{
            File f = new File(fajlnev);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document xml = db.parse(f);
            xml.normalize();
            NodeList nodeok = xml.getElementsByTagName(peldany.getClass().getSimpleName());
            egyedek = new Object[nodeok.getLength()];
            for(Integer i = 0; i < nodeok.getLength(); i++){
                Node node = nodeok.item(i);
                Element egyed = (Element)node;
                NodeList tulajdonsagok = egyed.getChildNodes();
                Map<HashMap<String, String>, HashMap<String, String>> tulajdonsagAdatok = new HashMap<HashMap<String, String>, HashMap<String, String>>();
                for(Integer j = 0; j < tulajdonsagok.getLength(); j++){
                    Node nodee = tulajdonsagok.item(j);
                    if(nodee.getNodeType() != Node.ELEMENT_NODE){
                        continue;
                    }
                    Element tulajdonsag = (Element)nodee;
                    String tulajdonsagNev = tulajdonsag.getNodeName();
                    String adat = tulajdonsag.getTextContent();
                    NamedNodeMap attrs = tulajdonsag.getAttributes();
                    HashMap <String, String> ertekAdatok = new HashMap<>();
                    ertekAdatok.put(tulajdonsagNev, adat);
                    HashMap <String, String> attributumok = new HashMap<>();
                    for(Integer k = 0; k < attrs.getLength(); k++){ 
                        Attr attr = (Attr)attrs.item(k);
                        String kulcs = attr.getName();
                        String ertek = attr.getValue();
                        attributumok.put(kulcs, ertek);
                    }
                    tulajdonsagAdatok.put(ertekAdatok, attributumok);
                }
                egyedek[i] = tulajdonsagAdatok;
            }
            System.out.println(Arrays.toString(egyedek));
        }
        catch(Exception ex){
            System.err.println("Hiba: " + ex.toString());
            System.out.println(Arrays.toString(ex.getStackTrace()));
        }
        return egyedek;
    }
    
}
