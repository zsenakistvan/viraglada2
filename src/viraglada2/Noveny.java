package viraglada2;

import annotation.getterFunctionName;
import java.util.AbstractMap;

public abstract class Noveny {
    @getterFunctionName(value = "getFajta", type = String.class)
    protected String fajta;
    @getterFunctionName(value = "getSzin", type = SZINEK.class)
    protected SZINEK szin;
    @getterFunctionName(value = "getIllat", type = AbstractMap.SimpleEntry.class)
    protected AbstractMap.SimpleEntry<Integer, String> illat;

    public Noveny() {
        this.fajta = "Lágyszárú";
        this.illat = ILLAT.SAV;
        this.szin = SZINEK.ZÖLD;
    }

    public Noveny(String fajta, SZINEK szin, AbstractMap.SimpleEntry<Integer, String> illat) {
        this.fajta = fajta;
        this.szin = szin;
        this.illat = illat;
    }

    public String getFajta() {
        return this.fajta;
    }

    public SZINEK getSzin() {
        return this.szin;
    }

    public AbstractMap.SimpleEntry<Integer, String> getIllat() {
        return this.illat;
    }

    public void setSzin(SZINEK szin) {
        this.szin = szin;
    }

    public void setIllat(AbstractMap.SimpleEntry<Integer, String> illat) {
        this.illat = illat;
    }
     
}
